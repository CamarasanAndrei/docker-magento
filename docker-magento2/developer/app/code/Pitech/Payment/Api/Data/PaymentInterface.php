<?php

namespace Pitech\Payment\Api\Data;

interface PaymentInterface
{
    /**
     * @param string $id
     * 
     * @return $this
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getId();

    /**
     * @param float $amount
     * 
     * @return $this
     */
    public function setAmountCaptured(float $amount);

    /**
     * @return float
     */
    public function getAmountCaptured(): float;

    /**
     * @param float $amount
     * 
     * @return $this
     */
    public function setAmountRefunded(float $amount);

    /**
     * @return float
     */
    public function getAmountRefunded(): float;

    /**
     * @param bool $captured
     * 
     * @return $this
     */
    public function setCaptured(bool $captured);

    /**
     * @return bool
     */
    public function getCaptured(): bool;

    /**
     * @param string $currency
     * 
     * @return $this
     */
    public function setCurrency(string $currency);

    /**
     * @return string
     */
    public function getCurrency(): string;

    /**
     * @param string $description
     * 
     * @return $this
     */
    public function setDescription(string $description);

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $brand
     * 
     * @return $this
     */
    public function setBrand(string $brand);

    /**
     * @return string
     */
    public function getBrand(): string;

    /**
     * @param string $expYear
     * 
     * @return $this
     */
    public function setExpYear(string $expYear);

    /**
     * @return string
     */
    public function getExpYear(): string;

    /**
     * @param string $last4
     * 
     * @return $this
     */
    public function setLast4(string $last4);

    /**
     * @return string
     */
    public function getLast4(): string;
}