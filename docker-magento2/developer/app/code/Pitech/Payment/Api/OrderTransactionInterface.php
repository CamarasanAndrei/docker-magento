<?php

namespace Pitech\Payment\Api;
/**
 * Class OrderTransactionInterface
 * @api
 */
interface OrderTransactionInterface
{
    /**
     * Create transaction for order
     *
     * @param string $orderId
     * @param \Pitech\Payment\Api\Data\PaymentInterface $payment
     * @return \Magento\Sales\Api\Data\TransactionInterface
     */
    public function create(
      $orderId,
      \Pitech\Payment\Api\Data\PaymentInterface $payment
    );
}