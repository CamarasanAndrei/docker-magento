<?php

namespace Pitech\Payment\Model\Order;

use Magento\Framework\Model\AbstractModel;
use Pitech\Payment\Api\Data\PaymentInterface;

class Payment extends AbstractModel implements PaymentInterface
{
    /** @var string */
    protected $id;

    /** @var float */
    protected $amountCaptured;

    /** @var float */
    protected $amountRefunded;

    /** @var bool */
    protected $captured;

    /** @var string */
    protected $currency;
    
    /** @var string */
    protected $description;

    /** @var string */
    protected $brand;

    /** @var string */
    protected $expYear;

    /** @var string */
    protected $last4;

    /**
     * @param string $id
     * 
     * @return $this
     */
    public function setId($id)
    {
        $this->setData('id', $id);

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * @param float $amount
     * 
     * @return $this
     */
    public function setAmountCaptured(float $amount)
    {
        $this->setData('amountCaptured', $amount);
        
        return $this;
    }

    /**
     * @return float
     */
    public function getAmountCaptured(): float
    {
        return $this->getData('amountCaptured');
    }

    /**
     * @param float $amount
     * 
     * @return $this
     */
    public function setAmountRefunded(float $amount)
    {
        $this->setData('amountRefunded', $amount);
        
        return $this;
    }

    /**
     * @return float
     */
    public function getAmountRefunded(): float
    {
        return $this->getData('amountRefunded');
    }

    /**
     * @param bool $captured
     * 
     * @return $this
     */
    public function setCaptured(bool $captured)
    {
        $this->setData('captured', $captured);
        
        return $this;
    }

    /**
     * @return bool
     */
    public function getCaptured(): bool
    {
        return $this->getData('captured');
    }

    /**
     * @param string $currency
     * 
     * @return $this
     */
    public function setCurrency(string $currency)
    {
        $this->setData('currency', $currency);
        
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->getData('currency');
    }

    /**
     * @param string $description
     * 
     * @return $this
     */
    public function setDescription(string $description)
    {
        $this->setData('description', $description);
        
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getData('description');
    }

    /**
     * @param string $brand
     * 
     * @return $this
     */
    public function setBrand(string $brand)
    {
        $this->setData('brand', $brand);
        
        return $this;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->getData('brand');
    }

    /**
     * @param string $expYear
     * 
     * @return $this
     */
    public function setExpYear(string $expYear)
    {
        $this->setData('expYear', $expYear);
        
        return $this;
    }

    /**
     * @return string
     */
    public function getExpYear(): string
    {
        return $this->getData('expYear');
    }

    /**
     * @param string $last4
     * 
     * @return $this
     */
    public function setLast4(string $last4)
    {
        $this->setData('last4', $last4);
        
        return $this;
    }

    /**
     * @return string
     */
    public function getLast4(): string
    {
        return $this->getData('last4');
    }
}