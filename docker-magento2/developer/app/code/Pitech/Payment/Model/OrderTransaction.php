<?php

namespace Pitech\Payment\Model;

use Magento\Sales\Api\Data\OrderInterfaceFactory;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Sales\Model\Order\Payment\Transaction;

use Pitech\Payment\Api\OrderTransactionInterface;

use Psr\Log\LoggerInterface;

/**
 * Class OrderTransaction
 * 
 * @api
 */
class OrderTransaction implements OrderTransactionInterface
{
    /** @var BuilderInterface */
    protected $transactionBuilder;

    /** @var OrderInterfaceFactory */
    protected $orderFactory;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * Constructor
     *
     * @param BuilderInterface $builderInterface
     * @param OrderInterfaceFactory $orderInterfaceFactory
     * @param LoggerInterface $loggerInterface
     */
    public function __construct(
      BuilderInterface $builderInterface,
      OrderInterfaceFactory $orderInterfaceFactory,
      LoggerInterface $loggerInterface
    ) {
        $this->transactionBuilder = $builderInterface;
        $this->orderFactory = $orderInterfaceFactory;
        $this->logger = $loggerInterface;
    }

    /**
     * Undocumented function
     *
     * @param string $orderId
     * @param \Pitech\Payment\Api\Data\PaymentInterface $payment
     * @return void
     */
    public function create($orderId, \Pitech\Payment\Api\Data\PaymentInterface $payment)
    {
        try {
            $order = $this->orderFactory->create()
                ->load($orderId);
            $paymentData = $payment->getData();
            //get payment object from order object
            $paymentModel = $order->getPayment();
            $paymentModel->setLastTransId($paymentData['id']);
            $paymentModel->setTransactionId($paymentData['id']);
            $paymentModel->setCcExpYear($paymentData['expYear']);
            $paymentModel->setCcType($paymentData['brand']);
            $paymentModel->setCcLast4($paymentData['last4']);
            $paymentModel->setAdditionalInformation(
                [Transaction::RAW_DETAILS => (array) $paymentData]
            );
            $formatedPrice = $order->getBaseCurrency()->formatTxt(
                $order->getGrandTotal()
            );
 
            $message = __('The authorized amount is %1.', $formatedPrice);
            //get the object of builder class
            $trans = $this->transactionBuilder;
            $transaction = $trans->setPayment($paymentModel)
            ->setOrder($order)
            ->setTransactionId($paymentData['id'])
            ->setAdditionalInformation(
                [Transaction::RAW_DETAILS => (array) $paymentData]
            )
            ->setFailSafe(true)
            //build method creates the transaction and returns the object
            ->build(Transaction::TYPE_CAPTURE);
 
            $paymentModel->addTransactionCommentsToOrder(
                $transaction,
                $message
            );
            $paymentModel->setParentTransactionId(null);
            $paymentModel->save();
            $order->save();
 
            return  $transaction->save();
        } catch (\Exception $e) {
            $this->logger->critical($e);
            throw new \Exception(
                __('Could not save transaction, see error log for details')
            );
        }
    }
}