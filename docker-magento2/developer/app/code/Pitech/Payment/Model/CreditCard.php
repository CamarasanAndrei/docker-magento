<?php

namespace Pitech\Payment\Model;

use Magento\Payment\Model\Method\AbstractMethod;

class CreditCard extends AbstractMethod
{
  const CODE = 'ccc';

  /**
   * Payment method code
   *
   * @var string
   */
  protected $_code = self::CODE;
}