import AWS from 'aws-sdk';

const eventBridge = new AWS.EventBridge();

export function emit(detail, detailType, source, eventBusName = 'default') {
  return eventBridge.putEvents({
    Entries: [
      {
        Detail: JSON.stringify(detail),
        DetailType: detailType,
        Source: source,
        EventBusName: eventBusName
      }
    ]
  }).promise();
}