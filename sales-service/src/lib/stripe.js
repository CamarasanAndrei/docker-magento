const stripe = require('stripe')(process.env.STRIPE_SECRET, {
  maxNetworkRetries: 2,
});
import createError from 'http-errors';

export async function createCardToken(cardNumber, cardName, expMonth, expYear, cvc) {
  try {
    const token = await stripe.tokens.create({
      card: {
        number: cardNumber,
        name: cardName,
        exp_month: expMonth,
        exp_year: expYear,
        cvc
      }
    });
    return token;
  } catch (error) {
    throw new createError.InternalServerError(error);
  }
}

export async function authorizeCharge(token, amount, currency, description = 'TESTING DATA AWS') {
  try {
    const charge = await stripe.charges.create({
      amount,
      currency,
      description,
      source: token,
      capture: false,
    });
    return charge;
  } catch (error) {
    throw new createError.InternalServerError(error);
  }
}

export async function captureCharge(chargeId) {
  try {
    const charge = await stripe.charges.capture(chargeId);
    return charge;
  } catch (error) {
    throw new createError.InternalServerError(error);
  }
}