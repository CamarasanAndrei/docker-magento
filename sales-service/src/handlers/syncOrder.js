import createError from 'http-errors';
import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import { getSaleById } from './getSale';
import { emit } from '../lib/eventBus';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function syncOrder(event, context) {
  console.log(event.detail);
  const order = event.detail;
  let sale = await getSaleById(order.cartId);
  sale.order = order;
  sale.status = order.status.toUpperCase();
  const params = {
    TableName: process.env.SALES_TABLE_NAME,
    Key: { id: order.cartId },
    UpdateExpression: 'set #order = :order, #status = :status',
    ExpressionAttributeValues: {
      ':order': order,
      ':status': sale.status,
    },
    ExpressionAttributeNames: {
      '#status': 'status',
      '#order': 'order'
    },
    ReturnValues: 'ALL_NEW'
  };

  try {
    await dynamodb.update(params).promise();
    if (sale.status === 'PENDING' && sale.billing.method.method === 'ccc') {
      console.log(sale);
      await emit(sale, 'CaptureSale', 'local.aws');
    }
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
}

export const handler = commonMiddleware(syncOrder);