import createError from 'http-errors';
import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import { createCardToken, authorizeCharge } from '../lib/stripe';
import { emit } from '../lib/eventBus';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function authorizePayment(event, context) {
  const { ccDetails: { cardNumber, cardName, expMonth, expYear, cvc }, totals, id, cartId, billing } = event.detail;
  const cardTokent = await createCardToken(cardNumber, cardName, expMonth, expYear, cvc);
  const charge = await authorizeCharge(cardTokent.id, totals.grand_total * 100, totals.base_currency_code.toLowerCase());
  const params = {
    TableName: process.env.SALES_TABLE_NAME,
    Key: { id },
    UpdateExpression: 'set charge = :charge',
    ExpressionAttributeValues: {
      ':charge': charge
    },
    ReturnValues: 'ALL_NEW'
  };

  try {
    await dynamodb.update(params).promise();
    await emit({ cartId, billing}, 'AsyncMagentoOrder', 'local.aws');
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
}

export const handler = commonMiddleware(authorizePayment);