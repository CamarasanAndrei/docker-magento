import commonMiddleware from '../lib/commonMiddleware';

async function getSales(event, context) {
  return {
    statusCode: 200,
    body: JSON.stringify({
      response: 'OK'
    })
  }
}

export const handler = commonMiddleware(getSales);