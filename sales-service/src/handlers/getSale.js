import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

export async function getSaleById(id) {
  let sale;

  try {
    const result = await dynamodb.get({
      TableName: process.env.SALES_TABLE_NAME,
      Key: { id }
    }).promise();
    sale = result.Item;
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }

  if (!sale) {
    throw new createError.NotFound(`Cart with ID "${id} doesn't exist!"`)
  }

  return sale;
}

async function getSale(event, context) {
  const { cartId } = event.pathParameters;
  const sale = await getSaleById(cartId);

  return {
    statusCode: 200,
    body: JSON.stringify(sale)
  }
}

export const handler = commonMiddleware(getSale);