import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import axios from 'axios';

async function placeMagentoOrder(event, context) {
  const { cartId, billing } = event.detail;
  const apiEndpoint = [process.env.MAGENTO_PROTOCOL + process.env.MAGENTO_DOMAIN, process.env.MAGENTO_API_PLACE_ORDER];
  const url = apiEndpoint.join('/').replace('{cartId}', cartId);
  
  try {
    await axios.post(url, {
      email: billing.address.email,
      paymentMethod: billing.method,
      billing_address: billing.address,
      crossdomain: true
    });
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
}

export const handler = commonMiddleware(placeMagentoOrder);


