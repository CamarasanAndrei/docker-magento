import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import axios from 'axios';

async function createMagentoInvoice(event, context) {
  const { orderId } = event.detail;
  const apiEndpoint = [process.env.MAGENTO_PROTOCOL + process.env.MAGENTO_DOMAIN, process.env.MAGENTO_API_CREATE_INVOICE];
  const url = apiEndpoint.join('/').replace('{orderId}', orderId);
  
  try {
    await axios.post(url, {}, {
      headers: {
        'Authorization': `Bearer ${process.env.MAGENTO_ACCESS_KEY}`
      }
    });
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
}

export const handler = commonMiddleware(createMagentoInvoice);


