import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';
import axios from 'axios';

async function createMagentoTransaction(event, context) {
  const { charge, orderId } = event.detail;
  const apiEndpoint = [process.env.MAGENTO_PROTOCOL + process.env.MAGENTO_DOMAIN, process.env.MAGENTO_API_CREATE_TRANSACTION];
  const url = apiEndpoint.join('/').replace('{orderId}', orderId);
  
  try {
    await axios.post(url,
      {
        payment: {
          id: charge.id,
          amount_captured: charge.amount_captured,
          amount_refunded: charge.amount_refunded,
          captured: charge.captured,
          currency: charge.currency,
          description: charge.description,
          brand: charge.payment_method_details.card.brand,
          exp_year: charge.payment_method_details.card.exp_year,
          last4: charge.payment_method_details.card.last4
        }
      },
      {
        headers: {
          'Authorization': `Bearer ${process.env.MAGENTO_ACCESS_KEY}`
        }
      }
    );
  } catch (error) {
    console.log(error);
    throw new createError.InternalServerError(error);
  }
}

export const handler = commonMiddleware(createMagentoTransaction);


