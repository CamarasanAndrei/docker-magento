# Serverless showcase for Magento order handling


## What's included
* **docker-magento2**: an instance of Magento 2.3.5.
* **nuxt-showcase**: a storefront for Magento developed with NuxtJs. 
* **sales-service**: IaaC developed with Serverless Framework. Containes different functions for Magento order handling.

