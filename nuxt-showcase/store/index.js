import filter from 'lodash/filter'
import merge from 'lodash/merge'

const addressMock = {
  region: 'New York',
  region_id: 43,
  region_code: 'NY',
  country_id: 'US',
  street: [
    '123 Oak Ave'
  ],
  postcode: '10577',
  city: 'Purchase',
  firstname: 'Andrei',
  lastname: 'Camarasan',
  email: 'camarasan.andrei92@gmail.com',
  telephone: '512-555-1111'
}

const defaultCartValue = {
  id: '',
  quoteId: '',
  items: [],
  totals: {},
  shipping: {
    address: {
      ...addressMock
    },
    method: {
      shipping_carrier_code: '',
      shipping_method_code: ''
    }
  },
  billing: {
    address: {
      ...addressMock
    },
    method: {
      method: ''
    }
  },
  customer: {},
  ccDetails: {
    cardNumber: '',
    cardName: '',
    expMonth: '',
    expYear: '',
    cvc: ''
  }
}

export const state = () => ({
  categories: [],
  rootCatId: 2,
  catDepth: 3,
  cart: {
    ...defaultCartValue
  },
  availableShippingMethods: [],
  availablePaymentMethods: [],
  order: {}
})

export const actions = {
  async nuxtServerInit ({ dispatch }, context) {
    await dispatch('getCategories')
  },
  async getCategories ({ getters, commit }) {
    try {
      const categoriesList = await this.$axios.$get(this.$config.magento.url + this.$config.magento.endpoints.categoriesList, {
        rootCategoryId: getters.getRootCatId,
        depth: getters.getCatDepth
      })
      commit('setCategoriesList', categoriesList)
      return categoriesList
    } catch (error) {
      return error
    }
  },
  async initCart ({ commit }) {
    try {
      const cartId = await this.$axios.$post(this.$config.magento.url + this.$config.magento.endpoints.initCart, {
        crossdomain: true
      })
      commit('setCartId', cartId)
      return cartId
    } catch (error) {
      return error
    }
  },
  async addProduct ({ getters, commit, dispatch }, { sku }) {
    if (!getters.getCartId) {
      await dispatch('initCart')
    }
    try {
      const cartItem = await this.$axios.$post(this.$config.magento.url + this.$config.magento.endpoints.addProduct.replace('{cartId}', getters.getCartId), {
        cartItem: {
          sku,
          qty: 1,
          quote_id: getters.getCartId
        },
        crossdomain: true
      })
      commit('setCartItem', cartItem)
      commit('setQuoteId', cartItem.quote_id)
      return cartItem
    } catch (error) {
      return error
    }
  },
  async getProducts (_, { catId }) {
    try {
      const products = await this.$axios.$get(this.$config.magento.url + this.$config.magento.endpoints.productsList.replace('{catId}', catId), {
        crossdomain: true
      })
      return products.items
    } catch (error) {
      return []
    }
  },
  async estimateShippingMethods ({ getters, commit }, address) {
    try {
      const shippingMethods = await this.$axios.$post(this.$config.magento.url + this.$config.magento.endpoints.estimateShippingMethods.replace('{cartId}', getters.getCartId), {
        address
      })
      commit('setAvailableShippingMethods', shippingMethods)
      return shippingMethods
    } catch (error) {
      return []
    }
  },
  async saveShippingInfo ({ commit, getters }) {
    try {
      const paymentData = await this.$axios.$post(this.$config.magento.url + this.$config.magento.endpoints.saveShippingInfo.replace('{cartId}', getters.getCartId), {
        addressInformation: {
          shipping_address: getters.getCartShippingAddress,
          ...getters.getCartShippingMethod
        }
      })
      commit('setAvailablePaymentMethods', paymentData.payment_methods)
      commit('setCartTotals', paymentData.totals)
      return paymentData
    } catch (error) {
      return {}
    }
  },
  async placeOrder ({ getters, commit }) {
    try {
      const cart = getters.getCart
      cart.id = cart.quoteId
      const order = await this.$axios.$post(this.$config.aws.url + this.$config.aws.endpoints.placeOrder.replace('{cartId}', getters.getCartId), {
        cart,
        crossdomain: true
      })
      commit('setOrder', order)
      commit('setCart', { ...defaultCartValue })
      return order
    } catch (error) {
      return {}
    }
  }
}

export const mutations = {
  setCategoriesList (state, categories) {
    state.categories = filter(categories.children_data, function mapper (s) {
      return s.is_active
    })
  },
  setCartId (state, id) {
    state.cart.id = id
  },
  setCartItems (state, items) {
    state.cart.items = items
  },
  setCartItem (state, item) {
    state.cart.items.push(item)
  },
  setCartTotals (state, totals) {
    state.cart.totals = totals
  },
  setCartShippingAddress (state, address) {
    state.cart.shipping.address = address
  },
  setCartBillingAddress (state, address) {
    state.cart.billing.address = address
  },
  setCartShippingMethod (state, method) {
    state.cart.shipping.method = method
  },
  setCartBillingMethod (state, method) {
    state.cart.billing.method = method
  },
  setCartShipping (state, shipping) {
    state.cart.shipping = shipping
  },
  setCartBilling (state, billing) {
    state.cart.billing = billing
  },
  setAvailablePaymentMethods (state, methods) {
    state.availablePaymentMethods = methods
  },
  setAvailableShippingMethods (state, methods) {
    state.availableShippingMethods = methods
  },
  setOrder (state, order) {
    state.order = order
  },
  setCart (state, cart) {
    state.cart = cart
  },
  setQuoteId (state, quoteId) {
    state.cart.quoteId = quoteId
  },
  setCreditCardDetails (state, { cardNumber, cardName, expMonth, expYear, cvc }) {
    state.cart.ccDetails.cardNumber = cardNumber
    state.cart.ccDetails.cardName = cardName
    state.cart.ccDetails.expMonth = expMonth
    state.cart.ccDetails.expYear = expYear
    state.cart.ccDetails.cvc = cvc
  }
}

export const getters = {
  getRootCatId (state) {
    return state.rootCatId
  },
  getCatDepth (state) {
    return state.catDepth
  },
  getCategoriesList (state) {
    return state.categories
  },
  getCart (state) {
    return merge({}, state.cart)
  },
  getCartId (state) {
    return state.cart.id
  },
  getCartItems (state) {
    return state.cart.items
  },
  getCartTotals (state) {
    return state.cart.totals
  },
  getCartShipping (state) {
    return state.cart.shipping
  },
  getCartShippingAddress (state) {
    return merge({}, state.cart.shipping.address)
  },
  getCartShippingMethod (state) {
    return merge({}, state.cart.shipping.method)
  },
  getCartBilling (state) {
    return state.cart.billing
  },
  getCartBillingAddress (state) {
    return merge({}, state.cart.billing.address)
  },
  getCartBillingMethod (state) {
    return merge({}, state.cart.billing.method)
  },
  getAvailablePaymentMethods (state) {
    return state.availablePaymentMethods
  },
  getAvailableShippingMethods (state) {
    return state.availableShippingMethods
  },
  getOrder (state) {
    return state.order
  },
  getQuoteId (state) {
    return state.cart.quoteId
  },
  getCreditCardDetails (state) {
    return state.cart.ccDetails
  }
}
