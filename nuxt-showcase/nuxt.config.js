export default {
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'nuxt-showcase',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto&display=swap'
      },
      {
        rel: 'stylesheet',
        href: 'https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.min.css'
      }
    ]
  },
  
  loading: {
    color: '#ffb300'
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@/assets/css/style'
  ],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    '@/plugins/vue-material',
    { src: '@/plugins/vuex-persist', ssr: false },
    { src: '@/plugins/web-worker', ssr: false },
    '@/plugins/vue-media-query'
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    'nuxt-material-design-icons'
  ],

  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  axios: {},

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    extend (config, ctx) {
      if (ctx.isClient) {
       config.module.rules.push({
        test: /\.worker\.js$/,
        loader: 'worker-loader',
        exclude: /(node_modules)/
       })
      }
    }
  },

  publicRuntimeConfig: {
    magento: {
      url: 'http://local.magento',
      magentoCatalogMediaPath: '/pub/media/catalog/product',
      endpoints: {
        categoriesList: '/rest/V1/categories',
        productsList: '/rest/V1/products?searchCriteria[filterGroups][0][filters][0][field]=category_id&searchCriteria[filterGroups][0][filters][0][value]={catId}&searchCriteria[filterGroups][0][filters][0][conditionType]=eq&searchCriteria[sortOrders][0][field]=created_at&searchCriteria[sortOrders][0][direction]=DESC',
        initCart: '/rest/V1/guest-carts',
        addProduct: '/rest/V1/guest-carts/{cartId}/items',
        estimateShippingMethods: '/rest/V1/guest-carts/{cartId}/estimate-shipping-methods',
        saveShippingInfo: '/rest/V1/guest-carts/{cartId}/shipping-information'
      }
    },
    aws: {
      url: 'https://8t1d7it2z8.execute-api.eu-west-1.amazonaws.com/dev',
      endpoints: {
        placeOrder: '/sales/place/{cartId}',
        getOrder: '/sales/{cartId}',
        getOrders: '/sales'
      }
    }
  }
}
