import Vue from 'vue'
import VueMq from 'vue-mq'

Vue.use(VueMq, {
  breakpoints: {
    xsmall: 599,
    small: 959,
    medium: 1279,
    large: 1919,
    xlarge: Infinity
  }
})
